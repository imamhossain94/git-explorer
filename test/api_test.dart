import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:git_explorer/app/data/model/git_search_query_param.dart';
import 'package:git_explorer/app/network/api_exceptions.dart';
import 'package:git_explorer/app/network/api_response_callback.dart';
import 'package:git_explorer/app/network/base_client.dart';
import 'package:git_explorer/flavors/build_config.dart';
import 'package:git_explorer/flavors/env.dart';
import 'package:git_explorer/flavors/env_config.dart';
import 'package:http_mock_adapter/http_mock_adapter.dart';

void main() {
  // Ensure Flutter binding is initialized
  TestWidgetsFlutterBinding.ensureInitialized();

  // Set up mock method call handler for path_provider plugin
  TestDefaultBinaryMessengerBinding.instance.defaultBinaryMessenger
      .setMockMethodCallHandler(
      const MethodChannel('plugins.flutter.io/path_provider'),
          (MethodCall methodCall) async {
        return '.';
      });

  EnvConfig config = EnvConfig(
    appName: "Top Flutter Repositories", baseUrl: "https://api.github.com",
  );
  BuildConfig.instantiate(envType: Env.QA, envConfig: config);

  // adapter to mock dio
  final dioAdapter = DioAdapter(dio: BaseClient.dio);

  group('success api cases', () {
    test('successful GET api call', () async {

      // simulate successful get request
      dioAdapter.onGet('https://api.github.com/search/repositories', (server) {
        server.reply(200, {'test': 'Passed ✅'});
      });

      final callbacks = ApiResponseCallbacks(
        onSuccess: (response) {
          expect(response, isNotNull, reason: 'api response must not be null');
          expect(response.statusCode, 200, reason: 'status code must be 200');

          // Check if response data matches the expected data
          expect(response.data, {'test': 'Passed ✅'});
        },
        onError: (exception) {
          expect(exception, isNull, reason: 'api error must be null');
        },
      );

      // perform api call
      var endpoint = "${BaseClient.baseUrl}/search/repositories";
      var prams = GithubSearchQueryParam(
        searchKeyWord: 'flutter',
        pageNumber: 1,
        perPage: 10
      );
      await BaseClient.safeApiCall(
        endpoint,
        RequestType.get,
        queryParameters: prams.toJson(),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
        callbacks: callbacks,
      );

    });
  });
}
