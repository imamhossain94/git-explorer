import 'package:flutter_test/flutter_test.dart';
import 'package:git_explorer/app/data/local/my_shared_pref.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> main() async {
  TestWidgetsFlutterBinding.ensureInitialized();

  // Initialize the shared preferences with mock values
  SharedPreferences.setMockInitialValues({});

  // Initialize MySharedPref
  await MySharedPref.init();

  group('Shared Preferences Tests', () {
    test('Clear All Data', () async {
      // Set sorting option to 0
      await MySharedPref.setSortingOption(0);

      // Check if sorting option is set correctly
      expect(MySharedPref.getSortingOption(), 0);

      // Clear all data
      await MySharedPref.clear();

      // After clearing data, sorting option should return the default value (0)
      expect(MySharedPref.getSortingOption(), 0);
    });

    test('Test Read and Write Theme', () async {
      // Set theme to light (true)
      await MySharedPref.setThemeIsLight(true);

      // Read the theme value
      bool themeIsLight = MySharedPref.getThemeIsLight();

      // Verify if the read and write operations were successful
      expect(themeIsLight, true);
    });
  });
}
