
import 'package:dio/dio.dart';
import 'api_exceptions.dart';


class ApiResponseCallbacks {
  final Function(Response response) onSuccess;
  final Function(ApiException)? onError;
  final Function(int value, int progress)? onReceiveProgress;
  final Function(int value, int progress)? onSendProgress;
  final Function()? onLoading;

  ApiResponseCallbacks({
    required this.onSuccess,
    this.onError,
    this.onReceiveProgress,
    this.onSendProgress,
    this.onLoading,
  });
}