import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:git_explorer/config/theme/my_theme.dart';
import 'package:git_explorer/config/translations/localization_service.dart';
import 'package:git_explorer/flavors/build_config.dart';
import 'package:git_explorer/flavors/env.dart';
import 'package:git_explorer/flavors/env_config.dart';

import '/app/routes/app_pages.dart';
import 'bindings/initial_binding.dart';
import 'data/local/my_shared_pref.dart';

Future<void> app() async {
  final Env env = BuildConfig.instance.env;
  final EnvConfig config = BuildConfig.instance.config;

  // wait for bindings
  WidgetsFlutterBinding.ensureInitialized();

  // init shared preference
  await MySharedPref.init();

  runApp(ScreenUtilInit(
    designSize: const Size(375, 812),
    minTextAdapt: true,
    splitScreenMode: true,
    useInheritedMediaQuery: true,
    rebuildFactor: (old, data) => true,
    builder: (context, widget) {
      return GetMaterialApp(
        title: config.appName,
        useInheritedMediaQuery: true,
        debugShowCheckedModeBanner: env == Env.DEBUG,
        builder: (context, widget) {
          bool themeIsLight = MySharedPref.getThemeIsLight();
          return Theme(
            data: MyTheme.getThemeData(isLight: themeIsLight),
            child: MediaQuery(
              // prevent font from scaling (some people use big/small device fonts)
              // but we want our app font to still the same and dont get affected
              data: MediaQuery.of(context)
                  .copyWith(textScaler: const TextScaler.linear(1.0)),
              child: widget!,
            ),
          );
        },
        initialRoute: AppPages.INITIAL,
        initialBinding: InitialBinding(),
        // first screen to show when app is running
        getPages: AppPages.routes,
        // app screens
        locale: MySharedPref.getCurrentLocal(),
        // app language
        translations: LocalizationService
            .getInstance(), // localization services in app (controller app language)
      );
    },
  ));
}
