import 'package:get/get.dart';
import 'package:git_explorer/app/data/model/git_search_query_param.dart';
import 'package:git_explorer/app/network/api_response_callback.dart';

import '/app/data/remote/github_remote_data_source.dart';
import '/app/data/repository/github_repository.dart';

class GithubRepositoryImpl implements GithubRepository {
  final GithubRemoteDataSource _remoteSource =
      Get.find(tag: (GithubRemoteDataSource).toString());

  @override
  repositoryDetails(
      String userName, String repositoryName, ApiResponseCallbacks callbacks) {
    _remoteSource.repositoryDetails(userName, repositoryName, callbacks);
  }

  @override
  search(GithubSearchQueryParam param, ApiResponseCallbacks callbacks) {
    _remoteSource.searchProject(param, callbacks);
  }
}
