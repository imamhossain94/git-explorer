import 'package:git_explorer/app/data/model/git_search_query_param.dart';
import 'package:git_explorer/app/network/api_response_callback.dart';

abstract class GithubRepository {
  search(GithubSearchQueryParam param, ApiResponseCallbacks callbacks);
  repositoryDetails(String userName, String repositoryName, ApiResponseCallbacks callbacks);
}
