import 'package:dio/dio.dart';
import 'package:git_explorer/app/data/model/git_search_query_param.dart';
import 'package:git_explorer/app/network/api_response_callback.dart';
import 'package:git_explorer/app/network/base_client.dart';

import '/app/data/remote/github_remote_data_source.dart';

class GithubRemoteDataSourceImpl implements GithubRemoteDataSource {
  Dio get dioClient => BaseClient.dio;

  @override
  searchProject(
      GithubSearchQueryParam param, ApiResponseCallbacks callbacks) {
    var endpoint = "${BaseClient.baseUrl}/search/repositories";

    try {
      BaseClient.safeApiCall(endpoint, RequestType.get,
          queryParameters: param.toJson(), callbacks: callbacks);
    } catch (e) {
      rethrow;
    }
  }

  @override
  repositoryDetails(
      String userName, String repositoryName, ApiResponseCallbacks callbacks) {
    var endpoint = "${BaseClient.baseUrl}/repos/$userName/$repositoryName";
    try {
      BaseClient.safeApiCall(endpoint, RequestType.get, callbacks: callbacks);
    } catch (e) {
      rethrow;
    }
  }
}
