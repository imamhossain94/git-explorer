
enum Sort { stars, updated }
enum Order { asc, desc }

class GithubSearchQueryParam {
  String searchKeyWord;
  int perPage;
  int pageNumber;
  Sort sort;
  Order order;

  GithubSearchQueryParam({
    this.searchKeyWord = "flutter",
    this.perPage = 10,
    this.pageNumber = 1,
    this.sort = Sort.stars,
    this.order = Order.desc,
  });

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['q'] = searchKeyWord;
    data['per_page'] = perPage;
    data['page'] = pageNumber;
    data['sort'] = sort == Sort.stars ? 'stars' : 'updated';
    data['order'] = order == Order.asc ? 'asc' : 'desc';
    return data;
  }
}
