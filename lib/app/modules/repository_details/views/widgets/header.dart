import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:git_explorer/app/modules/repository_details/controllers/repository_details_controller.dart';
import 'package:git_explorer/config/theme/extension/header_theme_data.dart';
import 'package:intl/intl.dart';

class Header extends StatelessWidget {
  const Header({super.key});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    var controller = Get.find<RepositoryDetailsController>();
    return Container(
      height: 86.h,
      width: double.infinity,
      decoration: BoxDecoration(
        color: theme.primaryColor,
      ),
      child: Stack(
        fit: StackFit.expand,
        children: [
          Positioned(
            bottom: 10,
            right: 16.w,
            left: 16.w,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                //----------------Back Button----------------//
                InkWell(
                  onTap: ()=> Get.back(),
                  child: Ink(
                    child: Container(
                      height: 39.h,
                      width: 39.h,
                      decoration: theme.extension<HeaderThemeData>()?.decoration?.copyWith(
                          color: const Color(0x000D1117),
                          border: Border.all(color: Color(Get.isDarkMode ? 0xFF30363d : 0xFFd9dfe5), width: 0.5)
                      ),
                      child: SvgPicture.asset(
                        'assets/vectors/left.svg',
                        fit: BoxFit.none,
                        color: Get.isDarkMode ? Colors.white : Colors.black,
                        height: 10,
                        width: 10,
                      ),
                    ),
                  ),
                ),
                9.horizontalSpace,
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Obx(() => Text(
                        "${controller.item.value.fullName}",
                        style: theme.textTheme.bodyMedium?.copyWith(
                        ),
                        overflow: TextOverflow.ellipsis,
                      )),
                      Obx(()=> Text(
                        DateFormat('MM-dd-yyyy HH:mm').format(DateTime.parse(controller.item.value.updatedAt!)),
                        style: theme.textTheme.bodyMedium?.copyWith(
                          fontSize: 12.sp,
                        ),
                        overflow: TextOverflow.ellipsis,
                      ))
                    ],
                  ),
                )

              ],
            ),
          )
        ],
      ),
    );
  }
}
