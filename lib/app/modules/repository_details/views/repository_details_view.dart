import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:git_explorer/app/components/api_error_widget.dart';
import 'package:git_explorer/app/components/my_widgets_animator.dart';
import 'package:git_explorer/app/modules/repository_details/controllers/repository_details_controller.dart';
import 'package:git_explorer/app/modules/repository_details/views/widgets/header.dart';
import 'package:git_explorer/config/translations/strings_enum.dart';
import 'package:intl/intl.dart';


class RepositoryDetailsView extends GetView<RepositoryDetailsController> {
  const RepositoryDetailsView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          // ----------------------- Header ----------------------- //
          const Header(),

          // ----------------------- Repository details ----------------------- //
          GetBuilder<RepositoryDetailsController>(builder: (_) {
            return Expanded(
              child: MyWidgetsAnimator(
                apiCallStatus: controller.apiCallStatus,
                loadingWidget: () => const Center(
                  child: CupertinoActivityIndicator(),
                ),
                errorWidget: () => ApiErrorWidget(
                  message: Strings.internetError.tr,
                  retryAction: () => controller.repositoryDetails(),
                  padding: EdgeInsets.symmetric(horizontal: 20.w),
                ),
                successWidget: () => SizedBox(
                  height: double.infinity,
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [_owner(), _repositoryDetail()],
                    ),
                  ),
                )
              ),
            );
          }),
        ],
      ),
    );
  }

  Widget _owner() {
    return Container(
      padding: EdgeInsets.all(15.w),
      child: Row(
        children: [
          CachedNetworkImage(
            imageUrl: controller.item.value.owner!.avatarUrl!,
            imageBuilder: (context, imageProvider) => Container(
              height: 24.h,
              width: 24.h,
              decoration: BoxDecoration(
                color: Colors.blueGrey,
                borderRadius: BorderRadius.circular(6),
                image: DecorationImage(
                  image: imageProvider,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            placeholder: (context, url) => Center(
                child: SizedBox(
                    height: 10.h,
                    width: 10.h,
                    child: const CircularProgressIndicator(
                      strokeWidth: 1,
                    ))),
            errorWidget: (context, url, error) => const Icon(Icons.error),
          ),
          9.horizontalSpace,
          Text(
            controller.item.value.owner!.login!,
            style: Get.theme.textTheme.bodyMedium?.copyWith(),
            overflow: TextOverflow.ellipsis, // Set overflow to ellipsis
          ),
        ],
      ),
    );
  }

  Widget _repositoryDetail() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 0.h, horizontal: 15.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            controller.item.value.name!,
            style: Get.theme.textTheme.headlineLarge?.copyWith(),
            overflow: TextOverflow.ellipsis, // Set overflow to ellipsis
          ),
          8.verticalSpace,
          Text(
            controller.item.value.description.toString(),
            style: Get.theme.textTheme.bodyMedium
                ?.copyWith(), // Set overflow to ellipsis
          ),
          7.verticalSpace,
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(controller.item.value.language.toString(),
                  style: Get.theme.textTheme.bodyMedium?.copyWith(
                    color: const Color(0xFF4B4C4D),
                  )),
              9.horizontalSpace,
              SvgPicture.asset(
                'assets/vectors/star.svg',
                fit: BoxFit.contain,
                color: const Color(0xFFd9dfe5),
                height: 18,
                width: 18,
              ),
              9.horizontalSpace,
              Text(
                  "${NumberFormat.compact()
                      .format(controller.item.value.stargazersCount)} ${controller.item.value.stargazersCount! > 0 ? 'stars' : 'star'}",
                  style: Get.theme.textTheme.bodyMedium?.copyWith(
                    color: const Color(0xFF4B4C4D),
                  )),
              9.horizontalSpace,
              SvgPicture.asset(
                'assets/vectors/git_branch.svg',
                fit: BoxFit.contain,
                color: const Color(0xFFd9dfe5),
                height: 18,
                width: 18,
              ),
              9.horizontalSpace,
              Text(
                  "${NumberFormat.compact()
                      .format(controller.item.value.forksCount)} ${controller.item.value.forksCount! > 0 ? 'forks' : 'fork'}",
                  style: Get.theme.textTheme.bodyMedium?.copyWith(
                    color: const Color(0xFF4B4C4D),
                  )),
            ],
          ),
        ],
      ),
    );
  }



}
