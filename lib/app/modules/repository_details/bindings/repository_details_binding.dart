import 'package:get/get.dart';
import 'package:git_explorer/app/modules/repository_details/controllers/repository_details_controller.dart';


class RepositoryDetailsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RepositoryDetailsController>(
          () => RepositoryDetailsController(),
    );
  }
}