import 'package:get/get.dart';
import 'package:git_explorer/app/data/model/git_repository_details_response.dart';
import 'package:git_explorer/app/data/model/git_search_repository_response.dart';
import 'package:git_explorer/app/network/api_call_status.dart';
import 'package:git_explorer/app/network/api_response_callback.dart';
import 'package:git_explorer/app/network/base_client.dart';

import '/app/data/repository/github_repository.dart';


class RepositoryDetailsController extends GetxController {

  final GithubRepository _repository = Get.find(tag: (GithubRepository).toString());

  Rx<Items> item = Items().obs;

  ApiCallStatus apiCallStatus = ApiCallStatus.holding;

  @override
  void onInit() {
    var args = Get.arguments;
    if (args is Items) {
      item(args);
      repositoryDetails();
    }
    super.onInit();
  }

  void repositoryDetails() {
    final callbacks = ApiResponseCallbacks(
      onLoading: () {
        // *) indicate loading state
        apiCallStatus = ApiCallStatus.loading;
        update();
      },
      onSuccess: (response) {
        // Handle successful response
        var data = GitRepositoryDetailsResponse.fromJson(response.data);


        // *) indicate success state
        apiCallStatus = ApiCallStatus.success;
        update();
      },
      onError: (error) {
        // show error message to user
        BaseClient.handleApiError(error);
        // *) indicate error status
        apiCallStatus = ApiCallStatus.error;
        update();
      },
    );

    _repository.repositoryDetails(item.value.owner!.login!, item.value.name!, callbacks);
  }



}
