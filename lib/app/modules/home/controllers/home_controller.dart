import 'package:get/get.dart';
import 'package:git_explorer/app/data/local/my_shared_pref.dart';
import 'package:git_explorer/app/data/model/git_search_query_param.dart';
import 'package:git_explorer/app/data/model/git_search_repository_response.dart';
import 'package:git_explorer/app/data/repository/github_repository.dart';
import 'package:git_explorer/app/network/api_response_callback.dart';
import 'package:git_explorer/app/network/base_client.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class HomeController extends GetxController {
  final GithubRepository _repository =
      Get.find(tag: (GithubRepository).toString());

  RxInt totalRepositoryCount = 0.obs;
  RxInt selectedSortingOption = 0.obs;

  final _params = GithubSearchQueryParam(searchKeyWord: 'flutter');

  final PagingController<int, Items> pagingController =
      PagingController(firstPageKey: 1);

  @override
  void onInit() {
    onFilterChange(MySharedPref.getSortingOption());

    pagingController.addPageRequestListener((page) {
      if(page != 1) {
        initialSearch(page);
      }
    });

    super.onInit();
  }

  void onFilterChange(int? value) {
    selectedSortingOption(value);
    MySharedPref.setSortingOption(value!);
    if (value == 0) {
      _params.sort = Sort.stars;
      _params.order = Order.desc;
    } else if (value == 1) {
      _params.sort = Sort.stars;
      _params.order = Order.asc;
    } else if (value == 2) {
      _params.sort = Sort.updated;
      _params.order = Order.desc;
    } else if (value == 3) {
      _params.sort = Sort.updated;
      _params.order = Order.asc;
    }
    pagingController.itemList?.clear();
    initialSearch(1);
  }

  void initialSearch(int page) {
    _params.pageNumber = page;

    final callbacks = ApiResponseCallbacks(
      onSuccess: (response) {
        // Handle successful response
        var data = GitSearchRepositoryResponse.fromJson(response.data);

        if (data.items != null) {
          totalRepositoryCount(data.totalCount);
          final nextPageKey = page + 1;
          pagingController.appendPage(data.items!, nextPageKey);
        }

      },
      onError: (error) {
        // show error message to user
        pagingController.error(error.toString());
        BaseClient.handleApiError(error);
      },
    );

    _repository.search(_params, callbacks);
  }
}
