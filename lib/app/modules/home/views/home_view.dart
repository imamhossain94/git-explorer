
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:git_explorer/app/components/api_error_widget.dart';
import 'package:git_explorer/app/data/model/git_search_repository_response.dart';
import 'package:git_explorer/app/modules/home/controllers/home_controller.dart';
import 'package:git_explorer/config/translations/strings_enum.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import 'widgets/header.dart';
import 'widgets/item_git_repository.dart';
import 'widgets/repository_filter.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          // ----------------------- Header ----------------------- //
          const Header(),

          // ----------------------- Repository Filter Action ----------------------- //
          const RepositoryFilter(),

          // ----------------------- Repositories ----------------------- //
          GetBuilder<HomeController>(builder: (_) {
            return Expanded(
              child: RefreshIndicator(
                onRefresh: () => Future.sync(
                      () => controller.initialSearch(1),
                ),
                child: PagedListView<int, Items>(
                  pagingController: controller.pagingController,
                  builderDelegate: PagedChildBuilderDelegate<Items>(
                    itemBuilder: (context, item, index) =>
                        ItemGitRepository(data: item),
                    firstPageProgressIndicatorBuilder: (_) => const Center(
                      child: CupertinoActivityIndicator(),
                    ),
                    firstPageErrorIndicatorBuilder: (_) => ApiErrorWidget(
                      message: controller.pagingController.error,
                      retryAction: () => controller.pagingController.refresh(),
                      padding: EdgeInsets.symmetric(horizontal: 20.w),
                    ),
                    newPageProgressIndicatorBuilder: (_) => const Padding(
                      padding: EdgeInsets.only(
                        top: 16,
                        bottom: 16,
                      ),
                      child: Center(child: CupertinoActivityIndicator()),
                    ),
                    newPageErrorIndicatorBuilder: (_) => InkWell(
                      onTap: () => controller.pagingController.retryLastFailedRequest(),
                      child: const Padding(
                        padding: EdgeInsets.only(
                          top: 16,
                          bottom: 16,
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Something went wrong. Tap to try again.',
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Icon(
                              Icons.refresh,
                              size: 16,
                            ),
                          ],
                        ),
                      ),
                    )
                  ),
                ),
              ),
            );
          }),
        ],
      ),
    );
  }
}