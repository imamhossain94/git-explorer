import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:git_explorer/app/modules/home/controllers/home_controller.dart';
import 'package:intl/intl.dart';

class RepositoryFilter extends StatelessWidget {
  const RepositoryFilter({super.key});

  @override
  Widget build(BuildContext context) {
    Theme.of(context);
    var controller = Get.find<HomeController>();
    var sortingOptions = [
      'Most stars',
      'Fewest stars',
      'Recently updated',
      'Least recently updated',
    ];

    return Padding(
      padding: EdgeInsets.only(top: 10.h),
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 10.w, vertical: 5.h),
            padding: EdgeInsets.fromLTRB(12.w, 5.w, 5.w, 5.w),
            decoration: BoxDecoration(
              color: Color(Get.isDarkMode ? 0xFF21262d : 0xffeef1f4),
              borderRadius: BorderRadius.circular(8),
              border: Border.all(
                color: Color(Get.isDarkMode ? 0xFF30363d : 0xFFd9dfe5),
                width: 0.5,
              ),
            ),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                const Text("Repositories"),
                9.horizontalSpace,
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 5.w, vertical: 2.h),
                  decoration: BoxDecoration(
                    color: Color(Get.isDarkMode ? 0xFF40464f : 0xffe1e6ea),
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: Obx(() => Text(NumberFormat.compact()
                      .format(controller.totalRepositoryCount.value.toDouble()))),
                ),
              ],
            ),
          ),
          Expanded(
            child: DropdownButtonHideUnderline(
              child: DropdownButton2(
                customButton: Container(
                  margin: EdgeInsets.only(right: 10.w),
                  padding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 5.h),
                  decoration: BoxDecoration(
                    color: Color(Get.isDarkMode ? 0xFF21262d : 0xffeef1f4),
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(
                      color: Color(Get.isDarkMode ? 0xFF30363d : 0xFFd9dfe5),
                      width: 0.5,
                    ),
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        'assets/vectors/filter.svg',
                        fit: BoxFit.contain,
                        color: Get.isDarkMode ? Colors.white : Colors.black,
                        height: 18,
                        width: 18,
                      ),
                      9.horizontalSpace,
                      Expanded(
                        // Wrap the Text widget with Expanded
                        child: Obx(() => Text(
                          "Sort by: ${sortingOptions[controller.selectedSortingOption.value]}",
                          style: const TextStyle(
                              fontWeight: FontWeight.normal, fontSize: 14),
                          overflow: TextOverflow.ellipsis,
                        )),
                      ),
                    ],
                  ),
                ),
                items: [
                  ...sortingOptions.map((item) =>  DropdownMenuItem<int>(
                    value: sortingOptions.indexOf(item),
                    child: Text(item,
                        style: const TextStyle(fontWeight: FontWeight.normal, fontSize: 14)),
                  )),
                ],
                onChanged: controller.onFilterChange,
                buttonStyleData: ButtonStyleData(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(40),
                  ),
                ),
                dropdownStyleData: DropdownStyleData(
                  width: 160,
                  padding: const EdgeInsets.symmetric(vertical: 6),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: Color(Get.isDarkMode ? 0xFF21262d : 0xffeef1f4),
                  ),
                  offset: const Offset(50, -4),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
