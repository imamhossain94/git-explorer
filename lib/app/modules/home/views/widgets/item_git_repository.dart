import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:git_explorer/app/routes/app_pages.dart';
import 'package:intl/intl.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:git_explorer/app/data/model/git_search_repository_response.dart';


class ItemGitRepository extends StatelessWidget {
  final Items data;
  const ItemGitRepository({required this.data ,super.key});

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);

    return InkWell(
      onTap: () => Get.toNamed(Routes.REPOSITORY_DETAILS, arguments: data),
      child: Ink(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 10.w, vertical: 5.h),
          padding: EdgeInsets.symmetric(horizontal: 12.w, vertical: 10.h),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            border: Border.all(color: Color(Get.isDarkMode ? 0xFF30363d : 0xFFd9dfe5), width: 0.5)
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  CachedNetworkImage(
                    imageUrl: data.owner!.avatarUrl!,
                    imageBuilder: (context, imageProvider) => Container(
                      height: 39.h,
                      width: 39.h,
                      decoration: BoxDecoration(
                        color: Colors.blueGrey,
                        borderRadius: BorderRadius.circular(8),
                        image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    placeholder: (context, url) => Center(child: SizedBox(height: 10.h, width: 10.h, child: const CircularProgressIndicator(strokeWidth: 1,))),
                    errorWidget: (context, url, error) => const Icon(Icons.error),
                  ),
                  9.horizontalSpace,
                  Expanded( // Added Expanded to let the Text widget take remaining space
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          data.fullName!,
                          style: theme.textTheme.bodyMedium?.copyWith(),
                          overflow: TextOverflow.ellipsis, // Set overflow to ellipsis
                        ),
                        Text(
                          "Updated ${timeago.format(DateTime.parse(data.updatedAt!))}",
                          style: theme.textTheme.bodyMedium?.copyWith(
                            fontSize: 12.sp,
                          ),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              8.verticalSpace,
              Text(data.description ?? '', style: theme.textTheme.bodySmall, maxLines: 5, overflow: TextOverflow.ellipsis,),
              7.verticalSpace,

              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(data.language.toString(), style: theme.textTheme.bodyMedium?.copyWith(color: const Color(0xFF4B4C4D),)),
                  9.horizontalSpace,
                  SvgPicture.asset(
                    'assets/vectors/star.svg',
                    fit: BoxFit.contain,
                    color: const Color(0xFFd9dfe5),
                    height: 18,
                    width: 18,
                  ),
                  9.horizontalSpace,
                  Text(NumberFormat.compact().format(data.stargazersCount), style: theme.textTheme.bodyMedium?.copyWith(color: const Color(0xFF4B4C4D),)),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}