import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:git_explorer/config/theme/extension/header_theme_data.dart';
import 'package:git_explorer/config/theme/my_theme.dart';
import 'package:git_explorer/config/translations/localization_service.dart';
import 'package:git_explorer/flavors/build_config.dart';
import 'package:git_explorer/flavors/env.dart';
import 'package:git_explorer/flavors/env_config.dart';

class Header extends StatelessWidget {
  const Header({super.key});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final Env env = BuildConfig.instance.env;
    final EnvConfig config = BuildConfig.instance.config;
    return Container(
      height: 86.h,
      width: double.infinity,
      decoration: BoxDecoration(
        color: theme.primaryColor,
      ),
      child: Stack(
        fit: StackFit.expand,
        children: [
          Positioned(
            bottom: 10,
            right: 16.w,
            left: 16.w,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                //----------------GitHub Button----------------//
                InkWell(
                  onTap: null,
                  child: Ink(
                    child: Container(
                      height: 39.h,
                      width: 39.h,
                      decoration: theme.extension<HeaderThemeData>()?.decoration?.copyWith(
                        color: const Color(0x000D1117),
                        border: Border.all(color: Color(Get.isDarkMode ? 0xFF30363d : 0xFFd9dfe5), width: 0.5)
                      ),
                      child: SvgPicture.asset(
                        'assets/vectors/github.svg',
                        fit: BoxFit.none,
                        color: Get.isDarkMode ? Colors.white : Colors.black,
                        height: 10,
                        width: 10,
                      ),
                    ),
                  ),
                ),
                9.horizontalSpace,
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        config.appName,
                        style: theme.textTheme.bodyMedium?.copyWith(
                        ),
                        overflow: TextOverflow.ellipsis,
                      ),
                      Text(
                        env.name,
                        style: theme.textTheme.bodyMedium?.copyWith(
                          fontSize: 12.sp,
                        ),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),

                //----------------Theme Button----------------//
                InkWell(
                  onTap: () => MyTheme.changeTheme(),
                  child: Ink(
                    child: Container(
                      height: 39.h,
                      width: 39.h,
                      decoration: theme.extension<HeaderThemeData>()?.decoration?.copyWith(
                          color: const Color(0x000D1117),
                          border: Border.all(color: Color(Get.isDarkMode ? 0xFF30363d : 0xFFd9dfe5), width: 0.5)
                      ),
                      child: SvgPicture.asset(
                        Get.isDarkMode ? 'assets/vectors/moon.svg' : 'assets/vectors/sun.svg',
                        fit: BoxFit.none,
                        color: Get.isDarkMode ? Colors.white : Colors.black,
                      ),
                    ),
                  ),
                ),

                10.horizontalSpace,

                //----------------Language Button----------------//
                InkWell(
                  onTap: () => LocalizationService.updateLanguage(
                    LocalizationService.getCurrentLocal().languageCode == 'ar' ? 'en' : 'ar',
                  ),
                  child: Ink(
                    child: Container(
                      height: 39.h,
                      width: 39.h,
                      decoration: theme.extension<HeaderThemeData>()?.decoration?.copyWith(
                          color: const Color(0x000D1117),
                          border: Border.all(color: Color(Get.isDarkMode ? 0xFF30363d : 0xFFd9dfe5), width: 0.5)
                      ),
                      child: SvgPicture.asset(
                        'assets/vectors/language.svg',
                        fit: BoxFit.none,
                        color: Get.isDarkMode ? Colors.white : Colors.black,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
