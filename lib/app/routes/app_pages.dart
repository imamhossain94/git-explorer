import 'package:get/get.dart';
import 'package:git_explorer/app/modules/repository_details/bindings/repository_details_binding.dart';
import 'package:git_explorer/app/modules/repository_details/views/repository_details_view.dart';

import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => const HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.REPOSITORY_DETAILS,
      page: () => const RepositoryDetailsView(),
      binding: RepositoryDetailsBinding(),
    ),
  ];
}
