import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:git_explorer/config/translations/strings_enum.dart';

class ApiErrorWidget extends StatelessWidget {
  const ApiErrorWidget(
      {super.key,
      required this.message,
      required this.retryAction,
      this.padding});

  final String message;
  final Function retryAction;
  final EdgeInsets? padding;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding,
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(message, textAlign: TextAlign.center,),
            10.verticalSpace,
            ElevatedButton(
              onPressed: () => retryAction(),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 25.w),
                child: Text(Strings.retry.tr, style: const TextStyle(fontWeight: FontWeight.normal),),
              ),
            )
          ],
        ),
      ),
    );
  }
}
