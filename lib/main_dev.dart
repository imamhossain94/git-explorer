import '/flavors/build_config.dart';
import '/flavors/env_config.dart';
import 'app/app.dart';
import 'flavors/env.dart';

void main() {
  EnvConfig config = EnvConfig(
    appName: "Top Flutter Repositories",
    baseUrl: "https://api.github.com",
  );

  BuildConfig.instantiate(
    envType: Env.DEV,
    envConfig: config,
  );

  // Run app
  app();
}