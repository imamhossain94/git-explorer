import 'package:flutter/material.dart';

class HeaderThemeData extends ThemeExtension<HeaderThemeData> {
  final BoxDecoration? decoration;

  const HeaderThemeData({
    this.decoration,
  });

  @override
  ThemeExtension<HeaderThemeData> copyWith() {
    return HeaderThemeData(
      decoration: decoration,
    );
  }

  @override
  ThemeExtension<HeaderThemeData> lerp(covariant ThemeExtension<HeaderThemeData>? other, double t) {
    if (other is! HeaderThemeData) {
      return this;
    }

    return HeaderThemeData(
      decoration: BoxDecoration.lerp(decoration, other.decoration, t) ?? BoxDecoration(borderRadius: BorderRadius.circular(8)), // If lerp returns null, use an empty BoxDecoration
    );
  }

}