import '/flavors/env_config.dart';
import '/flavors/env.dart';

class BuildConfig {
  late final Env env;
  late final EnvConfig config;
  bool _lock = false;

  static final BuildConfig instance = BuildConfig._internal();

  BuildConfig._internal();

  factory BuildConfig.instantiate({
    required Env envType,
    required EnvConfig envConfig,
  }) {
    if (instance._lock) return instance;

    instance.env = envType;
    instance.config = envConfig;
    instance._lock = true;

    return instance;
  }
}
