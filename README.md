# Top Flutter Repositories

## Overview
This Flutter application showcases the most starred GitHub repositories by searching with the keyword 'Flutter'. It is developed using the MVVM architecture and repository patterns, ensuring separation of concerns and maintainability. GetX is utilized as the state management solution, providing efficient state handling and updating. Additionally, dependency injection principles are followed to ensure modularity and testability. The project is structured to support four build flavors, enabling easy configuration and customization for different environments.

|      Main Screen       |   Main Screen Collapsed   | 
|:----------------------:|:-------------------------:|
| ![](previews/home.jpg) | ![](previews/details.jpg) |



## TODO
- [x] Fetch repository list from the GitHub API using `Flutter` as the query keyword.
- [x] Utilize the `dio_cache_interceptor` package to store API responses, reducing network requests (no more frequently than once every 30 minutes) and improving performance.
- [x] Implement pagination by scrolling using the `infinite_scroll_pagination` package, fetching 10 items each time.
- [x] Implement sorting by either the last updated date-time or star count, and add a sorting button/icon.
- [x] Persist the selected sorting option across app sessions.
- [x] Design the Home page to display the repository list.
- [x] Design the Repo details page to display repository details.

## Instructions

There are four app flavors available, allowing the creation of multiple versions of the app from the same codebase. Each flavor can have distinct behaviors and appearances. 

* DEBUG `flutter run`
* DEV   `flutter run --flavor dev lib/main_dev.dart`
* PROD  `flutter run --flavor dev lib/main_prod.dart`
* QA    `flutter run --flavor dev lib/main_qa.dart`


## Author

#### Md. Imam Hossain

You can also follow my GitHub & GitLab Profile to stay updated about my latest projects:

[![GitHub Follow](https://img.shields.io/badge/Connect-imamhossain94-blue.svg?logo=Github&longCache=true&style=social&label=Follow)](https://github.com/imamhossain94/)
[![GitLab Follow](https://img.shields.io/badge/Connect-imamhossain94-blue.svg?logo=Gitlab&longCache=true&style=social&label=Follow)](https://gitlab.com/imamhossain94/)

If you liked the repo then kindly support it by giving it a star ⭐!

Copyright (c) 2023 MD. IMAM HOSSAIN
